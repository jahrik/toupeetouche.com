Phaser 3 Finite-State Machine Example
=====================================

Click `Show` in the header to see your app live. Updates to your code will instantly deploy and update live.

**Glitch** is the friendly community where you'll build the app of your dreams. Glitch lets you instantly create, remix, edit, and host an app, bot or site, and you can invite collaborators or helpers to simultaneously edit code with you.

Find out more [about Glitch](https://glitch.com/about).

Phaser 3 vs 2
-------------

This app uses version 3 of Phaser. Version 2 is still actively used and there are many pieces of documentation that refer to the version 2 API instead of version 3. Be sure to double check the version number for guides and documentation you use!

- [Phaser 3 API Documentation](https://photonstorm.github.io/phaser3-docs/index.html)
- [Making your first Phaser 3 game](https://phaser.io/tutorials/making-your-first-phaser-3-game)
- [Getting Started with Phaser 3](https://phaser.io/tutorials/getting-started-phaser3/index)

Your Project
------------

On the front-end,
- edit `public/client.js` to start making the game of your dreams
- edit `public/style.css` and `views/index.html` to update the styling of the webpage itself
- drag in `assets`, like images or music, to add them to your project

On the back-end,
- your app starts at `server.js`
- update the phaser version in `package.json`

Credits
=======
- Graphics by [ArMM1998](https://opengameart.org/content/zelda-like-tilesets-and-sprites)
